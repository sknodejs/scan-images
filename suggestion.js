var MIN_WIDTH = 140;
//  w:h
//  4:4 = 1
//  4:3 = 1.33
// 16:9 = 1.78 
var MAX_ASPECT_RATIO = 2.5;
var MIN_ASPECT_RATIO = 0.6;

// tmpData.sort(compare);
// a,b: image info objects
function compare(a, b) {
    return b.area - a.area;
};

// returns a filtered array 
// of image infos
function filter(infos){
	function myfilter(element) {
		if (element.width <= MIN_WIDTH) {
			return false;
		}
		
		var ratio = element.width/element.height;
		
		if (ratio > MAX_ASPECT_RATIO){
			return false;
		}
		if (ratio < MIN_ASPECT_RATIO){
			return false;
		}
		
  		return true;
	}
	
	var filtered =  infos.filter(myfilter);
	
	return filtered;
}


// returns the bigest image 
// from an array of image infos
function bigimage(infos) {
	infos.sort(compare);
    return infos[0];
};

function suggestion(imgs){
	
		// url ends at .jpg and may followed by request parameters 
		// reg exp (.*)jpg(\?.*)
		/*
		var pattern1 = /(.*)jpg$/;
		var pattern2 = /(.*)jpg(\?.*)/;
		function onlyjpg(element) {
			if (element.match(pattern1)){
				return true; 
			}
			if (element.match(pattern2)){
				return true; 
			}
				return false;
		}
		*/
		
		imgs.forEach(function(src) {
			var h = src.height;
			var w = src.width;
			var a = h * w;
			
			src.area = a;
		});
		
		var filtered = filter(imgs);
		
		var big = bigimage(filtered);
		
		return big;
};

module.exports = suggestion;

