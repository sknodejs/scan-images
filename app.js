var util = require('util');
var async = require('async');
var scanImages = require('./scanImages.js');
var suggestion = require('./suggestion.js');

process.on('uncaughtException', function(err) {
	console.error('EXCEPTION!');
	console.dir(err);
});

var urls = new Array();
 
// test redirect
// 0
urls.push("http://bit.ly/JNLmqz");  

// example links 
urls.push("http://us.lrd.yahoo.com/_ylc=X3oDMTBia3FyNjc4BHNlY3Rpb24DcnNz/SIG=1113ikj6o/**http%3A//www.cosplayhouse.com/");

// FIX: Issue #3 - https://bitbucket.org/sknodejs/scan-images/issue/3/the-3rd-test-url-does-not-return-a-json
urls.push("http://us.lrd.yahoo.com/_ylc=X3oDMTBia3FyNjc4BHNlY3Rpb24DcnNz/SIG=116kfgn67/**http%3A//www.horizonsidsystems.com/");
urls.push("http://us.lrd.yahoo.com/_ylc=X3oDMTBia3FyNjc4BHNlY3Rpb24DcnNz/SIG=114duk3t2/**http%3A//www.colonialamerica.com/");
// 4
urls.push("http://www.collegeonline.org/");
urls.push("http://news.yahoo.com/kerry-sarin-syria-assad-140543565.html");
urls.push("http://sports.yahoo.com/blogs/nfl-shutdown-corner/pam-oliver-suffered-serious-concussion-symptoms-five-days-150415828--nfl.html");
// 7
urls.push("http://music.yahoo.com/news/electric-zoo-canceled-two-deaths-during-festival-134023883-rolling-stone.html");
urls.push("http://www.vetstreet.com/our-pet-experts/use-your-spidey-sense-to-keep-pets-away-from-these-arachnids");
urls.push("http://www.nytimes.com/interactive/2013/09/05/movies/movies-20-young-directors.html?hp&_r=0");
// 10
urls.push("http://www.nytimes.com/2013/09/09/business/verizon-and-fcc-net-neutrality-battle-set-in-district-court.html?hp");
urls.push("http://www.nytimes.com/2013/09/08/world/middleeast/with-the-world-watching-syria-amassed-nerve-gas.html?hp");
urls.push("http://www.theguardian.com/world/2013/sep/08/syria-john-kerry-un-resolution");

// HEAVILY TESTED ON THIS LINK
// 13
urls.push("http://www.theguardian.com/politics/2013/sep/08/labour-unite-falkirk-bullying-claims");

urls.push("http://keepingscore.blogs.time.com/2013/09/08/nadal-djokovic-meet-again-for-u-s-open-title/");
urls.push("http://entertainment.time.com/2013/09/06/the-real-gangs-of-boardwalk-empire/");
// 16
urls.push("http://lightbox.time.com/2013/09/06/pictures-of-the-week-august-29-september-6/#1");
urls.push("http://www.nytimes.com/2013/10/06/us/rallies-nationwide-in-support-of-immigration-overhaul.html");

// test imgs from https://
urls.push("http://us.lrd.yahoo.com/_ylc=X3oDMTBia3FyNjc4BHNlY3Rpb24DcnNz/SIG=116kfgn67/**http%3A//www.horizonsidsystems.com/");

// Non existing URL
urls.push("http://www.foo.bar/");

// Broken or non existing references to images
urls.push("http://skounis.s3.amazonaws.com/mobile-apps/rssplus/test/no-images.html");



// example of usage
/*
scanImages(urls[1], function(err, data) {
	if (err) {
		console.error(err);
	} else {
		console.log(util.inspect(data, true, 6));
	}
});
*/

//testURL("http://us.lrd.yahoo.com/_ylc=X3oDMTBia3FyNjc4BHNlY3Rpb24DcnNz/SIG=116kfgn67/**http%3A//www.horizonsidsystems.com/");
testURL(urls[15]);
//testAll();

function testURL(url){
	scanImages(url, function(err, data) {
		if (err) {
			console.log("Scan finished with error:");
			console.error(err);
		} else {
			console.log("Scan finished. Returned data:");
			console.log(util.inspect(data, true, 6));
			console.log(suggestion(data.images));
		}
	});
}

function testAll(){
	urls.forEach(function(url) {
		testURL(url, function(err, data) {
			if (err) {
				console.log("Scan finished with error:");
				console.error(err);
			} else {
				console.log("Scan finished. Returned data:");
				console.log(util.inspect(data, true, 6));
			}
		});
	});
}
