var util = require('util');
var urlutils = require('url');
var http = require('http');
var https = require('https');
var request = require('request').defaults({maxRedirects:30})
var imagesize = require('imagesize');
var async = require('async');

function scanImages(url, callback) {
	var data = {};
	data.target = url;
	data.images = [];

	request(url, function (error, response, body) {
		if (error) {
			if (callback) callback(error);
		} else {
			var pageurl = response.request.uri.href;

			var pattern = /<img[^>]+src="([^">]+)/ig;
			//var pattern = /<img.*?src="(.*?\/([^\/"]*))/ig;
			//var pattern = /<img.+?src=[\"'](.+?)[\"'].+?>/ig;
			var match;

			//var count = body.match(pattern);
			//console.log(count.length);
			//return;

			while ((match = pattern.exec(body)) !== null) {
				// find all src="..." part of the <img> tags
				imgurl = match[1];

				if (imgurl.indexOf('://') == -1) {
					// convert relative URI to absolute URL
					imgurl = urlutils.resolve(pageurl, imgurl)
				}
				
				data.images.push({
					url: imgurl,
					width: 0,
					height: 0
				});
			}
			
			measureImage = function(item, callback) {
				console.log(item.url);
				var protocol = (item.url.indexOf('https') != -1) ? https : http;
				var imgrequest = protocol.get(item.url, function (response) {
					imagesize(response, function (err, result) {
						if (!err) {
							console.dir(result);
							item.width = result.width;
							item.height = result.height;
						} else {
							item.error = err;
							console.dir(err);
						}
						
						imgrequest.abort();

						callback();
					});
				}).on('error', function(err) {
					if (err.code == 'ENOTFOUND') {
						item.error = 'notfound';
					} else {
						item.error = err;
					}
					console.dir(item.error);
					callback();
				});
				imgrequest.setTimeout(10000, function(){
					console.log('TIMEOUT!');
					this.abort();
				});
			}

			async.eachLimit(data.images, 3, measureImage, function(err) {
				if (callback) callback(err, data);
			});
				
		}
	}).setMaxListeners(30);
}

module.exports = scanImages;