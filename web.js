var express = require('express');
var scanImages = require('./scanImages.js');
var suggestion = require('./suggestion.js'); 

var app = express();
app.use(express.bodyParser());
app.set('port', process.env.PORT || 8080);

// primer: http://localhost:8080/scan?url=http://www.collegeonline.org/
// https://www.monosnap.com/image/zd9p1wxPn3RJaTJAGRsuI56cD.png
app.all('/scan', function(req, res){
	var targetURL = req.body.url || req.query.url;

	if (typeof targetURL == 'undefined') {
		res.send(500, 'Incorrent URL or no URL');
	} else {
		scanImages(targetURL, function(err, data) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, data);
			}
		});
	}  
});

app.all('/suggest', function(req, res){
	var targetURL = req.body.url || req.query.url;

	if (typeof targetURL == 'undefined') {
		res.send(500, 'Incorrent URL or no URL');
	} else {
		scanImages(targetURL, function(err, data) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, suggestion(data.images));
			}
		});
	}  
});

app.listen(app.get('port'));
console.log('Listening on port ' + app.get('port'));
